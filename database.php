<?php
$host = 'localhost';
$username = 'php';
$password = 'php';
$database_name = 'first_db';

$create_database_query = file_get_contents('./resources/db/00_create_database.sql');
$create_users_table_query = file_get_contents('./resources/db/01_create_table_user.sql');
$create_list_table_query = file_get_contents('./resources/db/02_create_table_list.sql');
$insert_list_table_query = file_get_contents('./resources/db/03_insert_table_list.sql');

$conn = mysqli_connect($host, $username, $password) or die('Could not connect: ' . mysql_error());

if (!($conn -> select_db($database_name))) {
    $conn->query($create_database_query) or die("Error: " . $create_database_query . "\n" . $conn->error);
    $conn -> select_db($database_name) or die("'Could not connect:" . $database_name  . "\n" . $conn->error);
    $conn->query($create_users_table_query) or die("Error: " . $create_users_table_query .  "\n" . $conn->error);
    $conn->query($create_list_table_query) or die("Error: " . $create_list_table_query .  "\n" . $conn->error);
    $conn->query($insert_list_table_query) or die("Error: " . $insert_list_table_query .  "\n" . $conn->error);
}
$conn->close();
