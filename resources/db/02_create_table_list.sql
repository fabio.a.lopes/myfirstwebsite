CREATE TABLE IF NOT EXISTS list ( 
id INT auto_increment primary key, 
details TEXT not null, 
date_posted VARCHAR(30) not null, 
time_posted TIME not null, 
date_edited VARCHAR(30) not null, 
time_edited TIME not null, 
public VARCHAR(5) not null 
)
