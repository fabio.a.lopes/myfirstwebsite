<html>
	<head>
    <link rel="stylesheet" href="./assets/styles.css" />
		<title>My first PHP website</title>
	</head>
	<?php
    session_start();
    if ($_SESSION['user']) {
    } else {
        header("location:index.php");
    }
    $user = $_SESSION['user'];
    $id_exists = false;
    ?>
	<body>
		<h2>Home Page</h2>
		<p>Hello <?php print "$user"?>!</p>
		<a href="logout.php">Click here to logout</a><br/><br/>
		<a href="home.php">Return to Home page</a>
		<h2 style="align:center">Currently Selected</h2>
		<table style="border:1px" width="100%">
			<tr>
				<th>Id</th>
				<th>Details</th>
				<th>Post Time</th>
				<th>Edit Time</th>
				<th>Public Post</th>
			</tr>
			<?php
                if (!empty($_GET['id'])) {
                    $id = $_GET['id'];
                    $_SESSION['id'] = $id;
                    $id_exists = true;
                    $mysqli = mysqli_connect('localhost', 'php', 'php', 'first_db') or die('Could not connect: ' . mysql_error());
                    $query =  $mysqli -> query("Select * from list Where id='$id'"); // SQL Query
                    $count = mysqli_num_rows($query);
                    if ($count > 0) {
                        while ($row = $query -> fetch_array(MYSQLI_ASSOC)) {
                            print "<tr>";
                            print '<td style="align:center;">'. $row['id'] . "</td>";
                            print '<td style="align:center;">'. $row['details'] . "</td>";
                            print '<td style="align:center;">'. $row['date_posted']. " - ". $row['time_posted']."</td>";
                            print '<td style="align:center;">'. $row['date_edited']. " - ". $row['time_edited']. "</td>";
                            print '<td style="align:center;">'. $row['public']. "</td>";
                            print "</tr>";
                        }
                    } else {
                        $id_exists = false;
                    }
                }
            ?>
		</table>
		<br/>
		<?php
        if ($id_exists) {
            print '
		<form action="edit.php" method="POST">
			Enter new detail: <input type="text" name="details"/><br/>
			public post? <input type="checkbox" name="public[]" value="yes"/><br/>
			<input type="submit" value="Update List"/>
		</form>
		';
        } else {
            print '<h2 align="center">There is no data to be edited.</h2>';
        }
        ?>
	</body>
</html>

<?php
    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        $mysqli = mysqli_connect('localhost', 'php', 'php', 'first_db') or die('Could not connect: ' . mysql_error());
        $details = $mysqli -> real_escape_string($_POST['details']);
        $public = "no";
        $id = $_SESSION['id'];
        $time = strftime("%X");
        $date = strftime("%B %d, %Y");

        foreach ($_POST['public'] as $list) {
            if ($list != null) {
                $public = "yes";
            }
        }
        $mysqli -> query("UPDATE list SET details='$details', public='$public', date_edited='$date', time_edited='$time' WHERE id='$id'") ;

        header("location: home.php");
    }
?>