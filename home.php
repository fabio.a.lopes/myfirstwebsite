<html>
<head>
<link rel="stylesheet" href="./assets/styles.css" />
		<title>My first PHP website</title>
	</head>
    <?php
    session_start();
    if ($_SESSION['user']) {
    } else {
        header("location:index.php");
    }
    $user = $_SESSION['user'];
    ?>
    <body>
		<h2>Home Page</h2>
		<p>Hello <?php print "$user"?>!</p> <!--Displays user's name-->
		<a href="logout.php">Click here to logout</a><br/><br/>
		<form action="add.php" method="POST">
			Add more to list: <input type="text" name="details"/><br/>
			public post? <input type="checkbox" name="public[]" value="yes"/><br/>
			<input type="submit" value="Add to list"/>
		</form>
		<h2 style="align:center;">My list</h2>
		<table>
			<tr>
				<th>Id</th>
				<th>Details</th>
				<th>Post Time</th>
				<th>Edit Time</th>
				<th>Edit</th>
				<th>Delete</th>
				<th>Public Post</th>
			</tr>
			<?php
                $mysqli = mysqli_connect('localhost', 'php', 'php', 'first_db') or die('Could not connect: ' . mysql_error());
                $query = $mysqli -> query("Select * from list");
                while ($row = $query -> fetch_array(MYSQLI_ASSOC)) {
                    print "<tr>";
                    print '<td >'. $row['id'] . "</td>";
                    print '<td>'. $row['details'] . "</td>";
                    print '<td>'. $row['date_posted']. " - ". $row['time_posted']."</td>";
                    print '<td>'. $row['date_edited']. " - ". $row['time_edited']. "</td>";
                    print '<td><a href="edit.php?id='. $row['id'] .'">edit</a> </td>';
                    print '<td><a href="#" onclick="myFunction('.$row['id'].')">delete</a> </td>';
                    print '<td>'. $row['public']. "</td>";
                    print "</tr>";
                }
            ?>
		</table>
		<script>
			function myFunction(id)
			{
			var r=confirm("Are you sure you want to delete this record?");
			if (r==true)
			  {
			  	window.location.assign("delete.php?id=" + id);
			  }
			}
		</script>
	</body>
</html>